%{?cygwin_package_header}

Name:           cygwin-bzip2
Version:        1.0.6
Release:        4%{?dist}
Summary:        Cygwin port of bzip2 file compression utility

License:        BSD
Group:          Development/Libraries
URL:            http://www.bzip.org/
Source0:        http://www.bzip.org/%{version}/bzip2-%{version}.tar.gz
BuildArch:      noarch

Patch11:        bzip2-1.0.6-cygwin-dll.patch

BuildRequires:  cygwin32-filesystem
BuildRequires:  cygwin32-binutils
BuildRequires:  cygwin32-gcc
BuildRequires:  cygwin32

BuildRequires:  cygwin64-filesystem
BuildRequires:  cygwin64-binutils
BuildRequires:  cygwin64-gcc
BuildRequires:  cygwin64

%description
BZip2 library for the Cygwin cross-compiler toolchains.

%package -n cygwin32-bzip2
Summary:        BZip2 library for Cygwin32 toolchain
Group:          Development/Libraries
Provides:       %{name} = %{version}-%{release}
Obsoletes:      %{name} < %{version}-%{release}

%description -n cygwin32-bzip2
BZip2 library for the Cygwin i686 cross-compiler toolchain.

%package -n cygwin64-bzip2
Summary:        BZip2 library for Cygwin64 toolchain
Group:          Development/Libraries

%description -n cygwin64-bzip2
BZip2 library for the Cygwin x86_64 cross-compiler toolchain.

%{?cygwin_debug_package}


%prep
%setup -q -n bzip2-%{version}
%patch11 -p1 -b .dll


%build
mkdir -p build_32bit
pushd build_32bit
ln -s ../*.c ../*.h .
make -f ../Makefile-libbz2_so \
  CC="%{cygwin32_cc}" \
  AR="%{cygwin32_ar}" \
  RANLIB="%{cygwin32_ranlib}" \
  CFLAGS="%{cygwin32_cflags}" \
  %{?_smp_mflags} all
popd

mkdir -p build_64bit
pushd build_64bit
ln -s ../*.c ../*.h .
make -f ../Makefile-libbz2_so \
  CC="%{cygwin64_cc}" \
  AR="%{cygwin64_ar}" \
  RANLIB="%{cygwin64_ranlib}" \
  CFLAGS="%{cygwin64_cflags}" \
  %{?_smp_mflags} all
popd


%install
pushd build_32bit
install -D -m0755 cygbz2-1.dll $RPM_BUILD_ROOT%{cygwin32_bindir}/cygbz2-1.dll
install -D -m0644 bzlib.h $RPM_BUILD_ROOT%{cygwin32_includedir}/bzlib.h
install -D -m0644 libbz2.dll.a $RPM_BUILD_ROOT%{cygwin32_libdir}/libbz2.dll.a
popd

pushd build_64bit
install -D -m0755 cygbz2-1.dll $RPM_BUILD_ROOT%{cygwin64_bindir}/cygbz2-1.dll
install -D -m0644 bzlib.h $RPM_BUILD_ROOT%{cygwin64_includedir}/bzlib.h
install -D -m0644 libbz2.dll.a $RPM_BUILD_ROOT%{cygwin64_libdir}/libbz2.dll.a
popd


%files -n cygwin32-bzip2
%doc LICENSE
%{cygwin32_bindir}/cygbz2-1.dll
%{cygwin32_includedir}/bzlib.h
%{cygwin32_libdir}/libbz2.dll.a

%files -n cygwin64-bzip2
%doc LICENSE
%{cygwin64_bindir}/cygbz2-1.dll
%{cygwin64_includedir}/bzlib.h
%{cygwin64_libdir}/libbz2.dll.a


%changelog
* Tue Dec 05 2017 Yaakov Selkowitz <yselkowi@redhat.com> - 1.0.6-4
- rebuilt

* Sun Jun 30 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 1.0.6-3
- Rebuild for new Cygwin packaging scheme.
- Add cygwin64 package.

* Sun Aug 21 2011 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 1.0.6-2
- Remove Cygwin EXEs.

* Wed Mar 16 2011 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 1.0.6-1
- Initial spec file, largely based on mingw32-bzip2.
